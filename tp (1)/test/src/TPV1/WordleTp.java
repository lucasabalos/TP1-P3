package TPV1;

import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.plaf.ColorUIResource;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;

import java.applet.AudioClip;  //no borrar
import java.awt.Color;





import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormSpecs;
import com.jgoodies.forms.layout.RowSpec;

import TPV1.juegoInterno.estadoLetra;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.swing.JLabel;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;



public class WordleTp extends JFrame {

	private JPanel contentPane;
	private JPanel cuadricula_1;
	private String Intento1;
	private String Intento2,Intento3,Intento4,Intento5,Intento6,PalabraUsuario;
	private int intentos=6;
	private int EstaSobreLaPalabra=1;
	private AudioClip SonidoTecla, musicaFondo,pasaPalabra;
	private juegoInterno nuevo;
	private int segundos= 0 ,minutos = 0;
	JLabel lblNewLabel = new JLabel("00 : 00");
	JLabel lblNewLabel_1 = new JLabel("REC*");
	
	JTextPane Palabra1Letra1, Palabra1Letra2, Palabra1Letra3, Palabra1Letra4, Palabra1Letra5;
	JTextPane Palabra2Letra1, Palabra2Letra2, Palabra2Letra3, Palabra2Letra4, Palabra2Letra5;
	JTextPane Palabra3Letra1, Palabra3Letra2, Palabra3Letra3, Palabra3Letra4, Palabra3Letra5;
	JTextPane Palabra4Letra1, Palabra4Letra2, Palabra4Letra3, Palabra4Letra4, Palabra4Letra5;
	JTextPane Palabra5Letra1, Palabra5Letra2, Palabra5Letra3, Palabra5Letra4, Palabra5Letra5;
	JTextPane Palabra6Letra1, Palabra6Letra2, Palabra6Letra3, Palabra6Letra4, Palabra6Letra5;
//-----------------cronometro-------------------------------------------------
	private ActionListener accion= new ActionListener() {
	@Override
		public void actionPerformed(ActionEvent e) {
			segundos++;
			String ContadorDeTiempo="";
			
			if(segundos%2==0)
				lblNewLabel_1.setVisible(false);
			else
				lblNewLabel_1.setVisible(true);

			if(minutos<10)
				ContadorDeTiempo += "0" + minutos;
			else
				ContadorDeTiempo += minutos;

			ContadorDeTiempo += " : ";
			
			if(segundos<10)
				ContadorDeTiempo += "0" + segundos;
			else
				ContadorDeTiempo += segundos;

			if (segundos == 59) {
				segundos = 0;
				minutos++;
			}
			
			lblNewLabel.setText(ContadorDeTiempo);
		}
	};
    private Timer Crono = new Timer(1000,accion); //crean una accion cada cierto tiempo Timer(Milisengundos, MetodoDeAccion)
//---------------------------------------------------------------------------------
    
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					WordleTp frame = new WordleTp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		});
	}

	public WordleTp(){

		getContentPane().setFont(new Font("Bauhaus 93", Font.PLAIN, 10));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		contentPane = new JPanel();
		//contentPane = new FondoPanel();
		//contentPane.setForeground(new Color(255, 255, 255));
		//contentPane.setBackground(new Color(255, 255, 255));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		cuadricula_1 = new JPanel();
		//cuadricula_1 = new FondoPanel();
		//cuadricula_1.setForeground(new Color(169, 169, 169));
		cuadricula_1.setBackground(Color.BLUE);
		contentPane.add(cuadricula_1);
     
        
		FormLayout fl_cuadricula_1 = new FormLayout(new ColumnSpec[] {
				ColumnSpec.decode("50px"),
				ColumnSpec.decode("50px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("20px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("50px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("20px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("50px"),
				FormSpecs.LABEL_COMPONENT_GAP_COLSPEC,
				ColumnSpec.decode("20px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("20px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("50px"),
				FormSpecs.RELATED_GAP_COLSPEC,
				ColumnSpec.decode("20px"),},
			new RowSpec[] {
				RowSpec.decode("8dlu"),
				RowSpec.decode("30px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("30px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("30px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("30px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("30px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				RowSpec.decode("20px"),
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,
				FormSpecs.RELATED_GAP_ROWSPEC,
				FormSpecs.DEFAULT_ROWSPEC,});
		
		fl_cuadricula_1.setHonorsVisibility(false);
		cuadricula_1.setLayout(fl_cuadricula_1);
		
//--------------------------------------carga de variables--------------------------------------------------		
		SonidoTecla = java.applet.Applet.newAudioClip(getClass().getResource("/pruebas/button-30.wav"));
		musicaFondo = java.applet.Applet.newAudioClip(getClass().getResource("Funcionamiento Rm Gen Atmo.wav"));
		pasaPalabra = java.applet.Applet.newAudioClip(getClass().getResource("/pruebas/button-29.wav"));
		RefrescarLetras();
		nuevo = new juegoInterno();
		musicaFondo.loop();
        DateTimeFormatter Cronometro = DateTimeFormatter.ofPattern("HH:mm:ss");
        String Reloj = Cronometro.format(LocalDateTime.now());
		Crono.start();
		
//------------------------------------control evento Teclado------------------------------------------------
		KeyListener e = new KeyListener() {
			@Override
			public void keyReleased(KeyEvent e) {
				char LetraAdevolver= e.getKeyChar();
				if(LetraAdevolver=='a' || LetraAdevolver=='A') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='b' || LetraAdevolver=='B') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='c' || LetraAdevolver=='C') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='d' || LetraAdevolver=='D') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='e' || LetraAdevolver=='E') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='f' || LetraAdevolver=='F') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='g' || LetraAdevolver=='G') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='h' || LetraAdevolver=='H') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='i' || LetraAdevolver=='I') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='j' || LetraAdevolver=='J') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='k' || LetraAdevolver=='K') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(e.getKeyChar()=='l' || LetraAdevolver=='L') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='m' || LetraAdevolver=='M') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='n' || LetraAdevolver=='N') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='o' || LetraAdevolver=='O') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='p' || LetraAdevolver=='P') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='q' || LetraAdevolver=='Q') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='r' || LetraAdevolver=='R') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='s' || LetraAdevolver=='S') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='t' || LetraAdevolver=='T') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='u' || LetraAdevolver=='U') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='v' || LetraAdevolver=='V') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='w' || LetraAdevolver=='W') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='x' || LetraAdevolver=='X') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='y' || LetraAdevolver=='Y') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				if(LetraAdevolver=='z' || LetraAdevolver=='Z') 
					PalabraUsuario = nuevo.compilarPalabra(LetraAdevolver);
				
				RefrescarLetras();
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				if(PalabraUsuario!=null && PalabraUsuario.length()==5 && e.getKeyCode() == 10) {
					validarPalabra(PalabraUsuario);
					PalabraUsuario = nuevo.borrameEseCoso();
					intentos--;
					EstaSobreLaPalabra++;
					pasaPalabra.play();
					
				}
				
				if(PalabraUsuario!=null && PalabraUsuario.length() >=1 && e.getKeyCode()==8) 
					PalabraUsuario = nuevo.borrarletra();
				
				SonidoTecla.play();
			}
			
			@Override
			public void keyTyped(KeyEvent e) {	
			}
		};
		
		addKeyListener(e);
	}

//------------------------------------metodos privados-------------------------------------
	
	//Cada vez que se toque una tecla se refresca la pantalla para que aparescan las letras que el usuario presiono
	private void RefrescarLetras() {
		
		UIManager.put("Button.disabledText" , new ColorUIResource(Color.BLACK)); // cambia el color del texto de los botones cuando estan desabilitados (sino es gris)
		
		damePalabra();
		
		JLabel lblNewLabel_2 = new JLabel("Wordle!");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 24));
		cuadricula_1.add(lblNewLabel_2, "8, 2, 6, 1");
		
//		String palabraAAdivinar = nuevo.damePalabra();
		
		lblNewLabel.setFont(new Font("Berlin Sans FB", Font.PLAIN, 24));
		lblNewLabel.setForeground(Color.YELLOW);
		cuadricula_1.add(lblNewLabel, "16, 2, 5, 1");
		

		Palabra1Letra1 = new JTextPane();
		Palabra1Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra1Letra1.setRolloverEnabled(false);
		Palabra1Letra1.setRequestFocusEnabled(false);
//		Palabra1Letra1.setDefaultCapable(false);
		Palabra1Letra1.setFocusTraversalKeysEnabled(false);
		Palabra1Letra1.setFocusable(false);
//		Palabra1Letra1.setFocusPainted(false);
		Palabra1Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra1Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra1Letra1.setEnabled(false);
		cuadricula_1.add(Palabra1Letra1, "2, 6");
//		if (nuevo.compararPalabra(PalabraUsuario).equals(estadoLetra.Correcto)) {
//			Palabra1Letra1.setForeground(Color.GREEN);
//		}else if (nuevo.compararPalabra(PalabraUsuario).equals(estadoLetra.CorrectoMalUbicado)) {
//			Palabra1Letra1.setForeground(Color.YELLOW);
//		}else {
//			Palabra1Letra1.setForeground(Color.GRAY);
//		}
		Palabra1Letra1.setBackground(Color.WHITE);
				
		Palabra1Letra2 = new JTextPane();
		Palabra1Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra1Letra2.setRolloverEnabled(false);
		Palabra1Letra2.setRequestFocusEnabled(false);
//		Palabra1Letra2.setDefaultCapable(false);
		Palabra1Letra2.setFocusTraversalKeysEnabled(false);
		Palabra1Letra2.setFocusable(false);
//		Palabra1Letra2.setFocusPainted(false);
		Palabra1Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra1Letra2.setEnabled(false);
		Palabra1Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra1Letra2, "6, 6");
				

		Palabra1Letra3 = new JTextPane ();
		Palabra1Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra1Letra3.setRolloverEnabled(false);
		Palabra1Letra3.setRequestFocusEnabled(false);
// 		Palabra1Letra3.setDefaultCapable(false);
 		Palabra1Letra3.setFocusTraversalKeysEnabled(false);
		Palabra1Letra3.setFocusable(false);
//		Palabra1Letra3.setFocusPainted(false);
		Palabra1Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra1Letra3.setEnabled(false);
		Palabra1Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra1Letra3, "10, 6");
		
		Palabra1Letra4 = new JTextPane ();
		Palabra1Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra1Letra4.setRolloverEnabled(false);
		Palabra1Letra4.setRequestFocusEnabled(false);
//		Palabra1Letra4.setDefaultCapable(false);
		Palabra1Letra4.setFocusTraversalKeysEnabled(false);
		Palabra1Letra4.setFocusable(false);
//		Palabra1Letra4.setFocusPainted(false);
		Palabra1Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra1Letra4.setEnabled(false);
		Palabra1Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra1Letra4, "14, 6");
		
		Palabra1Letra5 = new JTextPane ();
		Palabra1Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra1Letra5.setRolloverEnabled(false);
		Palabra1Letra5.setRequestFocusEnabled(false);
//		Palabra1Letra5.setDefaultCapable(false);
		Palabra1Letra5.setFocusTraversalKeysEnabled(false);
		Palabra1Letra5.setFocusable(false);
//		Palabra1Letra5.setFocusPainted(false);
		Palabra1Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra1Letra5.setEnabled(false);
		Palabra1Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra1Letra5, "18, 6");
		
		JTextPane Palabra2Letra1 = new JTextPane();
		Palabra2Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra2Letra1.setRolloverEnabled(false);
		Palabra2Letra1.setRequestFocusEnabled(false);
//		Palabra2Letra1.setDefaultCapable(false);
		Palabra2Letra1.setFocusTraversalKeysEnabled(false);
		Palabra2Letra1.setFocusable(false);
//		Palabra2Letra1.setFocusPainted(false);
		Palabra2Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra2Letra1.setEnabled(false);
		Palabra2Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra2Letra1, "2, 10");
		
		JTextPane Palabra2Letra2 = new JTextPane();
		Palabra2Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra2Letra2.setRolloverEnabled(false);
		Palabra2Letra2.setRequestFocusEnabled(false);
//		Palabra2Letra2.setDefaultCapable(false);
		Palabra2Letra2.setFocusTraversalKeysEnabled(false);
		Palabra2Letra2.setFocusable(false);
//		Palabra2Letra2.setFocusPainted(false);
		Palabra2Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra2Letra2.setEnabled(false);
		Palabra2Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra2Letra2, "6, 10");
		
		JTextPane Palabra2Letra3 = new JTextPane();
		Palabra2Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra2Letra3.setRolloverEnabled(false);
		Palabra2Letra3.setRequestFocusEnabled(false);
//		Palabra2Letra3.setDefaultCapable(false);
		Palabra2Letra3.setFocusTraversalKeysEnabled(false);
		Palabra2Letra3.setFocusable(false);
//		Palabra2Letra3.setFocusPainted(false);
		Palabra2Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra2Letra3.setEnabled(false);
		Palabra2Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra2Letra3, "10, 10");
		
		JTextPane Palabra2Letra4 = new JTextPane();
		Palabra2Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra2Letra4.setRolloverEnabled(false);
		Palabra2Letra4.setRequestFocusEnabled(false);
//		Palabra2Letra4.setDefaultCapable(false);
		Palabra2Letra4.setFocusTraversalKeysEnabled(false);
		Palabra2Letra4.setFocusable(false);
//		Palabra2Letra4.setFocusPainted(false);
		Palabra2Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra2Letra4.setEnabled(false);
		Palabra2Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra2Letra4, "14, 10");
		
		JTextPane Palabra2Letra5 = new JTextPane();
		Palabra2Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra2Letra5.setRolloverEnabled(false);
		Palabra2Letra5.setRequestFocusEnabled(false);
//		Palabra2Letra5.setDefaultCapable(false);
		Palabra2Letra5.setFocusTraversalKeysEnabled(false);
		Palabra2Letra5.setFocusable(false);
//		Palabra2Letra5.setFocusPainted(false);
		Palabra2Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra2Letra5.setEnabled(false);
		Palabra2Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra2Letra5, "18, 10");
		
		JTextPane Palabra3Letra1 = new JTextPane();
		Palabra3Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra3Letra1.setRolloverEnabled(false);
		Palabra3Letra1.setRequestFocusEnabled(false);
//		Palabra3Letra1.setDefaultCapable(false);
		Palabra3Letra1.setFocusTraversalKeysEnabled(false);
		Palabra3Letra1.setFocusable(false);
//		Palabra3Letra1.setFocusPainted(false);
		Palabra3Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra3Letra1.setEnabled(false);
		Palabra3Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra3Letra1, "2, 14");
		
		JTextPane Palabra3Letra2 = new JTextPane();
		Palabra3Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra3Letra2.setRolloverEnabled(false);
		Palabra3Letra2.setRequestFocusEnabled(false);
//		Palabra3Letra2.setDefaultCapable(false);
		Palabra3Letra2.setFocusTraversalKeysEnabled(false);
		Palabra3Letra2.setFocusable(false);
//		Palabra3Letra2.setFocusPainted(false);
		Palabra3Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra3Letra2.setEnabled(false);
		Palabra3Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra3Letra2, "6, 14");
		
		
		JTextPane Palabra3Letra3 = new JTextPane();
		Palabra3Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra3Letra3.setRolloverEnabled(false);
		Palabra3Letra3.setRequestFocusEnabled(false);
//		Palabra3Letra3.setDefaultCapable(false);
		Palabra3Letra3.setFocusTraversalKeysEnabled(false);
		Palabra3Letra3.setFocusable(false);
//		Palabra3Letra3.setFocusPainted(false);
		Palabra3Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra3Letra3.setEnabled(false);
		Palabra3Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra3Letra3, "10, 14");
		
		JTextPane Palabra3Letra4 = new JTextPane();
		Palabra3Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra3Letra4.setRolloverEnabled(false);
		Palabra3Letra4.setRequestFocusEnabled(false);
//		Palabra3Letra4.setDefaultCapable(false);
		Palabra3Letra4.setFocusTraversalKeysEnabled(false);
		Palabra3Letra4.setFocusable(false);
//		Palabra3Letra4.setFocusPainted(false);
		Palabra3Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra3Letra4.setEnabled(false);
		Palabra3Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra3Letra4, "14, 14");
		
		JTextPane Palabra3Letra5 = new JTextPane();
		Palabra3Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra3Letra5.setRolloverEnabled(false);
		Palabra3Letra5.setRequestFocusEnabled(false);
//		Palabra3Letra5.setDefaultCapable(false);
		Palabra3Letra5.setFocusTraversalKeysEnabled(false);
		Palabra3Letra5.setFocusable(false);
//		Palabra3Letra5.setFocusPainted(false);
		Palabra3Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra3Letra5.setEnabled(false);
		Palabra3Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra3Letra5, "18, 14");
		
		JTextPane Palabra4Letra1 = new JTextPane();
		Palabra4Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra4Letra1.setRolloverEnabled(false);
		Palabra4Letra1.setRequestFocusEnabled(false);
//		Palabra4Letra1.setDefaultCapable(false);
		Palabra4Letra1.setFocusTraversalKeysEnabled(false);
		Palabra4Letra1.setFocusable(false);
//		Palabra4Letra1.setFocusPainted(false);
		Palabra4Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra4Letra1.setEnabled(false);
		Palabra4Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra4Letra1, "2, 18");
		
		JTextPane Palabra4Letra2 = new JTextPane();
		Palabra4Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra4Letra2.setRolloverEnabled(false);
		Palabra4Letra2.setRequestFocusEnabled(false);
//		Palabra4Letra2.setDefaultCapable(false);
		Palabra4Letra2.setFocusTraversalKeysEnabled(false);
		Palabra4Letra2.setFocusable(false);
//		Palabra4Letra2.setFocusPainted(false);
		Palabra4Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra4Letra2.setEnabled(false);
		Palabra4Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra4Letra2, "6, 18");
		
		JTextPane Palabra4Letra3 = new JTextPane();
		Palabra4Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra4Letra3.setRolloverEnabled(false);
		Palabra4Letra3.setRequestFocusEnabled(false);
//		Palabra4Letra3.setDefaultCapable(false);
		Palabra4Letra3.setFocusTraversalKeysEnabled(false);
		Palabra4Letra3.setFocusable(false);
//		Palabra4Letra3.setFocusPainted(false);
		Palabra4Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra4Letra3.setEnabled(false);
		Palabra4Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra4Letra3, "10, 18");
		
		JTextPane Palabra4Letra4 = new JTextPane();
		Palabra4Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra4Letra4.setRolloverEnabled(false);
		Palabra4Letra4.setRequestFocusEnabled(false);
//		Palabra4Letra4.setDefaultCapable(false);
		Palabra4Letra4.setFocusTraversalKeysEnabled(false);
		Palabra4Letra4.setFocusable(false);
//		Palabra4Letra4.setFocusPainted(false);
		Palabra4Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra4Letra4.setEnabled(false);
		Palabra4Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra4Letra4, "14, 18");
		
		JTextPane Palabra4Letra5 = new JTextPane();
		Palabra4Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra4Letra5.setRolloverEnabled(false);
		Palabra4Letra5.setRequestFocusEnabled(false);
//		Palabra4Letra5.setDefaultCapable(false);
		Palabra4Letra5.setFocusTraversalKeysEnabled(false);
		Palabra4Letra5.setFocusable(false);
//		Palabra4Letra5.setFocusPainted(false);
		Palabra4Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra4Letra5.setEnabled(false);
		Palabra4Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra4Letra5, "18, 18");
		
		JTextPane Palabra5Letra1 = new JTextPane();
		Palabra5Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra5Letra1.setRolloverEnabled(false);
		Palabra5Letra1.setRequestFocusEnabled(false);
//		Palabra5Letra1.setDefaultCapable(false);
		Palabra5Letra1.setFocusTraversalKeysEnabled(false);
		Palabra5Letra1.setFocusable(false);
//		Palabra5Letra1.setFocusPainted(false);
		Palabra5Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra5Letra1.setEnabled(false);
		Palabra5Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra5Letra1, "2, 22");
		
		JTextPane Palabra5Letra2 = new JTextPane();
		Palabra5Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra5Letra2.setRolloverEnabled(false);
		Palabra5Letra2.setRequestFocusEnabled(false);
//		Palabra5Letra2.setDefaultCapable(false);
		Palabra5Letra2.setFocusTraversalKeysEnabled(false);
		Palabra5Letra2.setFocusable(false);
//		Palabra5Letra2.setFocusPainted(false);
		Palabra5Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra5Letra2.setEnabled(false);
		Palabra5Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra5Letra2, "6, 22");
		
		JTextPane Palabra5Letra3 = new JTextPane();
		Palabra5Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra5Letra3.setRolloverEnabled(false);
		Palabra5Letra3.setRequestFocusEnabled(false);
//		Palabra5Letra3.setDefaultCapable(false);
		Palabra5Letra3.setFocusTraversalKeysEnabled(false);
		Palabra5Letra3.setFocusable(false);
//		Palabra5Letra3.setFocusPainted(false);
		Palabra5Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra5Letra3.setEnabled(false);
		Palabra5Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra5Letra3, "10, 22");
		
		JTextPane Palabra5Letra4 = new JTextPane();
		Palabra5Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra5Letra4.setRolloverEnabled(false);
		Palabra5Letra4.setRequestFocusEnabled(false);
//		Palabra5Letra4.setDefaultCapable(false);
		Palabra5Letra4.setFocusTraversalKeysEnabled(false);
		Palabra5Letra4.setFocusable(false);
//		Palabra5Letra4.setFocusPainted(false);
		Palabra5Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra5Letra4.setEnabled(false);
		Palabra5Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra5Letra4, "14, 22");
		
		JTextPane Palabra5Letra5 = new JTextPane();
		Palabra5Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra5Letra5.setRolloverEnabled(false);
		Palabra5Letra5.setRequestFocusEnabled(false);
//		Palabra5Letra5.setDefaultCapable(false);
		Palabra5Letra5.setFocusTraversalKeysEnabled(false);
		Palabra5Letra5.setFocusable(false);
//		Palabra5Letra5.setFocusPainted(false);
		Palabra5Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra5Letra5.setEnabled(false);
		Palabra5Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		cuadricula_1.add(Palabra5Letra5, "18, 22");
		
		JTextPane Palabra6Letra1 = new JTextPane();
		Palabra6Letra1.setVerifyInputWhenFocusTarget(false);
//		Palabra6Letra1.setRolloverEnabled(false);
		Palabra6Letra1.setRequestFocusEnabled(false);
//		Palabra6Letra1.setDefaultCapable(false);
		Palabra6Letra1.setFocusTraversalKeysEnabled(false);
		Palabra6Letra1.setFocusable(false);
//		Palabra6Letra1.setFocusPainted(false);
		Palabra6Letra1.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra6Letra1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra6Letra1.setEnabled(false);
		cuadricula_1.add(Palabra6Letra1, "2, 26");
		
		JTextPane Palabra6Letra2 = new JTextPane();
		Palabra6Letra2.setVerifyInputWhenFocusTarget(false);
//		Palabra6Letra2.setRolloverEnabled(false);
		Palabra6Letra2.setRequestFocusEnabled(false);
//		Palabra6Letra2.setDefaultCapable(false);
		Palabra6Letra2.setFocusTraversalKeysEnabled(false);
		Palabra6Letra2.setFocusable(false);
//		Palabra6Letra2.setFocusPainted(false);
		Palabra6Letra2.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra6Letra2.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra6Letra2.setEnabled(false);
		cuadricula_1.add(Palabra6Letra2, "6, 26");
		
		JTextPane Palabra6Letra3 = new JTextPane();
		Palabra6Letra3.setVerifyInputWhenFocusTarget(false);
//		Palabra6Letra3.setRolloverEnabled(false);
		Palabra6Letra3.setRequestFocusEnabled(false);
//		Palabra6Letra3.setDefaultCapable(false);
		Palabra6Letra3.setFocusTraversalKeysEnabled(false);
		Palabra6Letra3.setFocusable(false);
//		Palabra6Letra3.setFocusPainted(false);
		Palabra6Letra3.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra6Letra3.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra6Letra3.setEnabled(false);
		cuadricula_1.add(Palabra6Letra3, "10, 26");
		
		JTextPane Palabra6Letra4 = new JTextPane();
		Palabra6Letra4.setVerifyInputWhenFocusTarget(false);
//		Palabra6Letra4.setRolloverEnabled(false);
		Palabra6Letra4.setRequestFocusEnabled(false);
//		Palabra6Letra4.setDefaultCapable(false);
		Palabra6Letra4.setFocusTraversalKeysEnabled(false);
		Palabra6Letra4.setFocusable(false);
//		Palabra6Letra4.setFocusPainted(false);
		Palabra6Letra4.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra6Letra4.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra6Letra4.setEnabled(false);
		cuadricula_1.add(Palabra6Letra4, "14, 26");
		
		JTextPane Palabra6Letra5 = new JTextPane();
		Palabra6Letra5.setVerifyInputWhenFocusTarget(false);
//		Palabra6Letra5.setRolloverEnabled(false);
		Palabra6Letra5.setRequestFocusEnabled(false);
//		Palabra6Letra5.setDefaultCapable(false);
		Palabra6Letra5.setFocusTraversalKeysEnabled(false);
		Palabra6Letra5.setFocusable(false);
//		Palabra6Letra5.setFocusPainted(false);
		Palabra6Letra5.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, Color.RED, Color.GREEN, Color.YELLOW, Color.BLUE));
		Palabra6Letra5.setFont(new Font("Berlin Sans FB", Font.PLAIN, 14));
		Palabra6Letra5.setEnabled(false);
		cuadricula_1.add(Palabra6Letra5, "18, 26");
		if(Intento6!=null && Intento6.length()>=5) 
			Palabra6Letra5.setText(""+Intento6.charAt(4));
		if (EstaSobreLaPalabra == 1)
		Palabra1Letra1.setBackground(PintarDeBlanco());
		else
			Palabra1Letra1.setBackground(DameColor());
		if(Intento1!=null && Intento1.length()>=1) 
			Palabra1Letra1.setText(""+Intento1.charAt(0));
		else SwingUtilities.updateComponentTreeUI(contentPane);
		if (EstaSobreLaPalabra == 1)
			Palabra1Letra2.setBackground(PintarDeBlanco());
		else
			Palabra1Letra2.setBackground(DameColor());
		if(Intento1!=null && Intento1.length()>=2) 
			Palabra1Letra2.setText(""+Intento1.charAt(1));
		if (EstaSobreLaPalabra == 1)
			Palabra1Letra3.setBackground(PintarDeBlanco());
		else
			Palabra1Letra3.setBackground(DameColor());
		
		lblNewLabel_1.setFont(new Font("Berlin Sans FB", Font.PLAIN, 24));
		lblNewLabel_1.setForeground(Color.red);
		cuadricula_1.add(lblNewLabel_1, "2, 2");
		if(Intento1!=null && Intento1.length()>=3) 
			Palabra1Letra3.setText(""+Intento1.charAt(2));
		if (EstaSobreLaPalabra == 1)
			Palabra1Letra4.setBackground(PintarDeBlanco());
		else
			Palabra1Letra4.setBackground(DameColor());
		if(Intento1!=null && Intento1.length()>=4) 
			Palabra1Letra4.setText(""+Intento1.charAt(3));
		if (EstaSobreLaPalabra == 1)
			Palabra1Letra5.setBackground(PintarDeBlanco());
		else
			Palabra1Letra5.setBackground(DameColor());
		if(Intento1!=null && Intento1.length()>=5) 
			Palabra1Letra5.setText(""+Intento1.charAt(4));
		if (EstaSobreLaPalabra == 2)
			Palabra2Letra1.setBackground(PintarDeBlanco());
		else
			Palabra2Letra1.setBackground(DameColor());
		if(Intento2!=null && Intento2.length()>=1) 
			Palabra2Letra1.setText(""+Intento2.charAt(0));
		if (EstaSobreLaPalabra == 2)
			Palabra2Letra2.setBackground(PintarDeBlanco());
		else
			Palabra2Letra2.setBackground(DameColor());
		if(Intento2!=null && Intento2.length()>=2) 
			Palabra2Letra2.setText(""+Intento2.charAt(1));
		if (EstaSobreLaPalabra == 2)
			Palabra2Letra3.setBackground(PintarDeBlanco());
		else
			Palabra2Letra3.setBackground(DameColor());
		if(Intento2!=null && Intento2.length()>=3) 
			Palabra2Letra3.setText(""+Intento2.charAt(2));
		if (EstaSobreLaPalabra == 2)
			Palabra2Letra4.setBackground(PintarDeBlanco());
		else
			Palabra2Letra4.setBackground(DameColor());
		if(Intento2!=null && Intento2.length()>=4) 
			Palabra2Letra4.setText(""+Intento2.charAt(3));
		if (EstaSobreLaPalabra == 2)
			Palabra2Letra5.setBackground(PintarDeBlanco());
		else
			Palabra2Letra5.setBackground(DameColor());
		if(Intento2!=null && Intento2.length()>=5) 
			Palabra2Letra5.setText(""+Intento2.charAt(4));
		if (EstaSobreLaPalabra == 3)
			Palabra3Letra1.setBackground(PintarDeBlanco());
		else
			Palabra3Letra1.setBackground(DameColor());
		if(Intento3!=null && Intento3.length()>=1) 
			Palabra3Letra1.setText(""+Intento3.charAt(0));
		if (EstaSobreLaPalabra == 3)
			Palabra3Letra2.setBackground(PintarDeBlanco());
		else
			Palabra3Letra2.setBackground(DameColor());
		if(Intento3!=null && Intento3.length()>=2) 
			Palabra3Letra2.setText(""+Intento3.charAt(1));
		if (EstaSobreLaPalabra == 3)
			Palabra3Letra3.setBackground(PintarDeBlanco());
		else
			Palabra3Letra3.setBackground(DameColor());
		if(Intento3!=null && Intento3.length()>=3) 
			Palabra3Letra3.setText(""+Intento3.charAt(2));
		if (EstaSobreLaPalabra == 3)
			Palabra3Letra4.setBackground(PintarDeBlanco());
		else
			Palabra3Letra4.setBackground(DameColor());
		if(Intento3!=null && Intento3.length()>=4) 
			Palabra3Letra4.setText(""+Intento3.charAt(3));
		if (EstaSobreLaPalabra == 3)
			Palabra3Letra5.setBackground(PintarDeBlanco());
		else
			Palabra3Letra5.setBackground(DameColor());
		if(Intento3!=null && Intento3.length()>=5) 
			Palabra3Letra5.setText(""+Intento3.charAt(4));
		if (EstaSobreLaPalabra == 4)
			Palabra4Letra1.setBackground(PintarDeBlanco());
		else
			Palabra4Letra1.setBackground(DameColor());
		if(Intento4!=null && Intento4.length()>=1) 
			Palabra4Letra1.setText(""+Intento4.charAt(0));
		if (EstaSobreLaPalabra == 4)
			Palabra4Letra2.setBackground(PintarDeBlanco());
		else
			Palabra4Letra2.setBackground(DameColor());
		if(Intento4!=null && Intento4.length()>=2) 
			Palabra4Letra2.setText(""+Intento4.charAt(1));
		if (EstaSobreLaPalabra == 4)
			Palabra4Letra3.setBackground(PintarDeBlanco());
		else
			Palabra4Letra3.setBackground(DameColor());
		if(Intento4!=null && Intento4.length()>=3) 
			Palabra4Letra3.setText(""+Intento4.charAt(2));
		if (EstaSobreLaPalabra == 4)
			Palabra4Letra4.setBackground(PintarDeBlanco());
		else
			Palabra4Letra4.setBackground(DameColor());
		if(Intento4!=null && Intento4.length()>=4) 
			Palabra4Letra4.setText(""+Intento4.charAt(3));
		if (EstaSobreLaPalabra == 4)
			Palabra4Letra5.setBackground(PintarDeBlanco());
		else
			Palabra4Letra5.setBackground(DameColor());
		if(Intento4!=null && Intento4.length()>=5) 
			Palabra4Letra5.setText(""+Intento4.charAt(4));
		if (EstaSobreLaPalabra == 5)
			Palabra5Letra1.setBackground(PintarDeBlanco());
		else
			Palabra5Letra1.setBackground(DameColor());
		if(Intento5!=null && Intento5.length()>=1) 
			Palabra5Letra1.setText(""+Intento5.charAt(0));
		if (EstaSobreLaPalabra == 5)
			Palabra5Letra2.setBackground(PintarDeBlanco());
		else
			Palabra5Letra2.setBackground(DameColor());
		if(Intento5!=null && Intento5.length()>=2) 
			Palabra5Letra2.setText(""+Intento5.charAt(1));
		if (EstaSobreLaPalabra == 5)
			Palabra5Letra3.setBackground(PintarDeBlanco());
		else
			Palabra5Letra3.setBackground(DameColor());
		if(Intento5!=null && Intento5.length()>=3) 
			Palabra5Letra3.setText(""+Intento5.charAt(2));
		if (EstaSobreLaPalabra == 5)
			Palabra5Letra4.setBackground(PintarDeBlanco());
		else
			Palabra5Letra4.setBackground(DameColor());
		if(Intento5!=null && Intento5.length()>=4) 
			Palabra5Letra4.setText(""+Intento5.charAt(3));
		if (EstaSobreLaPalabra == 5)
			Palabra5Letra5.setBackground(PintarDeBlanco());
		else
			Palabra5Letra5.setBackground(DameColor());
		if(Intento5!=null && Intento5.length()>=5) 
			Palabra5Letra5.setText(""+Intento5.charAt(4));
		if (EstaSobreLaPalabra == 6)
			Palabra6Letra1.setBackground(PintarDeBlanco());
		else
			Palabra6Letra1.setBackground(DameColor());
		if(Intento6!=null && Intento6.length()>=1) 
			Palabra6Letra1.setText(""+Intento6.charAt(0));
		if (EstaSobreLaPalabra == 6)
			Palabra6Letra2.setBackground(PintarDeBlanco());
		else
			Palabra6Letra2.setBackground(DameColor());
		if(Intento6!=null && Intento6.length()>=2) 
			Palabra6Letra2.setText(""+Intento6.charAt(1));
		if (EstaSobreLaPalabra == 6)
			Palabra6Letra3.setBackground(PintarDeBlanco());
		else
			Palabra6Letra3.setBackground(DameColor());
		if(Intento6!=null && Intento6.length()>=3) 
			Palabra6Letra3.setText(""+Intento6.charAt(2));
		if (EstaSobreLaPalabra == 6)
			Palabra6Letra4.setBackground(PintarDeBlanco());
		else
			Palabra6Letra4.setBackground(DameColor());
		if(Intento6!=null && Intento6.length()>=4) 
			Palabra6Letra4.setText(""+Intento6.charAt(3));
		if (EstaSobreLaPalabra == 6)
			Palabra6Letra5.setBackground(PintarDeBlanco());
		else
			Palabra6Letra5.setBackground(DameColor());
				

	}

	//devuelve la palabra del usuario para mostrarlo por pantalla
	private void damePalabra() {
		if (intentos == 0) {
			GameOver newframe = new GameOver();
			newframe.setVisible(true);
			dispose();
		}
		
		if(PalabraUsuario!=null) {
			if (intentos==6) 
				Intento1 = PalabraUsuario.toUpperCase();
			if (intentos==5) 
				Intento2 = PalabraUsuario.toUpperCase();
			if (intentos==4) 
				Intento3 = PalabraUsuario.toUpperCase();
			if (intentos==3) 
				Intento4 = PalabraUsuario.toUpperCase();
			if (intentos==2) 
				Intento5 = PalabraUsuario.toUpperCase();
			if (intentos==1) 
				Intento6 = PalabraUsuario.toUpperCase();
		
		}
	}
	private void validarPalabra(String s) {
		estadoLetra []a = nuevo.compararPalabra(s);

		if (intentos == 6) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra1Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra1Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Mal))  
				Palabra1Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra1Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra1Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Mal))  
				Palabra1Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra1Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra1Letra3.setBackground(Color.YELLOW);
			if(a[2].equals(estadoLetra.Mal))  
				Palabra1Letra3.setBackground(Color.GRAY);
			
			
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra1Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra1Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Mal)) 
				Palabra1Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra1Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra1Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Mal)) 
				Palabra1Letra5.setBackground(Color.GRAY);
			
		}
		
		if (intentos == 5) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra2Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra2Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Correcto))  
				Palabra2Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra2Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra2Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Correcto))  
				Palabra2Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra2Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra2Letra3.setBackground(Color.YELLOW);
			if(a[2].equals(estadoLetra.Correcto))  
				Palabra2Letra3.setBackground(Color.GRAY);
			
			
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra2Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra2Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Correcto))  
				Palabra2Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra2Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra2Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Correcto))  
				Palabra2Letra5.setBackground(Color.GRAY);
			
		}
		
		if (intentos == 4) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra3Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra3Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Correcto))  
				Palabra3Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra3Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra3Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Correcto))  
				Palabra3Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra3Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra3Letra3.setBackground(Color.YELLOW);
			if(a[2].equals(estadoLetra.Correcto))  
				Palabra3Letra3.setBackground(Color.GRAY);
			
			
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra3Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra3Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Correcto))  
				Palabra3Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra3Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra3Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Correcto))  
				Palabra3Letra5.setBackground(Color.GRAY);
			
		}
		
		if (intentos == 3) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra4Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra4Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Correcto))  
				Palabra4Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra4Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra4Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Correcto))  
				Palabra4Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra4Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra4Letra3.setBackground(Color.YELLOW);
			if(a[2].equals(estadoLetra.Correcto))  
				Palabra4Letra3.setBackground(Color.GRAY);
			
			
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra4Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra4Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Correcto))  
				Palabra4Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra4Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra4Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Correcto))  
				Palabra4Letra5.setBackground(Color.GRAY);
			
		}
		
		if (intentos == 2) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra5Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra5Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Correcto)) 
				Palabra5Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra5Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra5Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Correcto)) 
				Palabra5Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra5Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra5Letra3.setBackground(Color.YELLOW);
			if(a[2].equals(estadoLetra.Correcto))  
				Palabra5Letra3.setBackground(Color.GRAY);
			
			
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra5Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra5Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Correcto))
				Palabra5Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra5Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra5Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Correcto)) 
				Palabra5Letra5.setBackground(Color.GRAY);
			
		}
		
		if (intentos == 1) {
			if (a[0].equals(estadoLetra.Correcto)) 
				Palabra6Letra1.setBackground(Color.GREEN);
			if(a[0].equals(estadoLetra.CorrectoMalUbicado))
				Palabra6Letra1.setBackground(Color.YELLOW);
			if(a[0].equals(estadoLetra.Correcto))  
				Palabra6Letra1.setBackground(Color.GRAY);
			
			
			if (a[1].equals(estadoLetra.Correcto)) 
				Palabra6Letra2.setBackground(Color.GREEN);
			if(a[1].equals(estadoLetra.CorrectoMalUbicado))
				Palabra6Letra2.setBackground(Color.YELLOW);
			if(a[1].equals(estadoLetra.Correcto)) 
				Palabra6Letra2.setBackground(Color.GRAY);
			
			
			if (a[2].equals(estadoLetra.Correcto)) 
				Palabra6Letra3.setBackground(Color.GREEN);
			if(a[2].equals(estadoLetra.CorrectoMalUbicado))
				Palabra6Letra3.setBackground(Color.YELLOW);
			else if(a[2].equals(estadoLetra.Correcto)) 
				Palabra6Letra3.setBackground(Color.GRAY);
			
		
			if (a[3].equals(estadoLetra.Correcto)) 
				Palabra6Letra4.setBackground(Color.GREEN);
			if(a[3].equals(estadoLetra.CorrectoMalUbicado))
				Palabra6Letra4.setBackground(Color.YELLOW);
			if(a[3].equals(estadoLetra.Correcto))  
				Palabra6Letra4.setBackground(Color.GRAY);
			
			
			if (a[4].equals(estadoLetra.Correcto)) 
				Palabra6Letra5.setBackground(Color.GREEN);
			if(a[4].equals(estadoLetra.CorrectoMalUbicado))
				Palabra6Letra5.setBackground(Color.YELLOW);
			if(a[4].equals(estadoLetra.Correcto))  
				Palabra6Letra5.setBackground(Color.GRAY);
			
		}
	}
	
		
	
	//color base las cuadros
	private Color DameColor() {
		return Color.WHITE;
	}
	
	//color de cuadro a ser seleccionada la fila
	private Color PintarDeBlanco() {
		return Color.WHITE;
	}
 
	class FondoPanel extends JPanel
    {
        private Image imagen;
        
        @Override
        public void paint(Graphics g)
        {
           imagen = new ImageIcon(getClass().getResource
        		   ("/pruebas/istockphoto-1301088529-612x612.jpg")).getImage();
            
           g.drawImage(imagen,0, 0, getWidth(), getHeight(),this);
            
           setOpaque(false);
            
           super.paint(g);
        }
    }

}

