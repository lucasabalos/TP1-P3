package TPV1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextPane;
import java.awt.Color;

public class Reglas extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Reglas frame = new Reglas();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Reglas() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 590, 328);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Reglas");
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 24));
		lblNewLabel.setBounds(236, 0, 136, 39);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Inicio newFrame = new Inicio();
				newFrame.setVisible(true);
				dispose();
			}
		});
		btnNewButton.setBounds(21, 12, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblNewLabel_1 = new JLabel("El juego propone una palabra oculta con cinco letras.\r\nLa dificultad reside en que, antes del primer ");
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 11));
		lblNewLabel_1.setBounds(21, 50, 519, 66);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("intento, no se ofrece ninguna pista. Y solo tiene 6 intentos. Si la letra est\u00E1 ubicada en el sitio ");
		lblNewLabel_2.setFont(new Font("Times New Roman", Font.BOLD, 11));
		lblNewLabel_2.setBounds(21, 102, 519, 39);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("adecuado, el cuadrado cambia al color verde; si est\u00E1 dentro de la palabra pero no bien ubicada,");
		lblNewLabel_3.setFont(new Font("Times New Roman", Font.BOLD, 11));
		lblNewLabel_3.setBounds(21, 139, 519, 39);
		contentPane.add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel(" el color ser\u00E1 amarillo; si no es ninguna de las dos opciones anteriores, la celda se colorea de gris. ");
		lblNewLabel_4.setFont(new Font("Times New Roman", Font.BOLD, 11));
		lblNewLabel_4.setBounds(21, 184, 519, 29);
		contentPane.add(lblNewLabel_4);
		
		JTextPane textPane = new JTextPane();
		textPane.setBackground(Color.GREEN);
		textPane.setBounds(31, 253, 30, 30);
		contentPane.add(textPane);
		
		JLabel lblNewLabel_5 = new JLabel("= ESTA");
		lblNewLabel_5.setBounds(71, 264, 48, 14);
		contentPane.add(lblNewLabel_5);
		
		JTextPane textPane_1 = new JTextPane();
		textPane_1.setBackground(Color.YELLOW);
		textPane_1.setBounds(146, 253, 30, 30);
		contentPane.add(textPane_1);
		
		JTextPane textPane_1_1 = new JTextPane();
		textPane_1_1.setBackground(Color.GRAY);
		textPane_1_1.setBounds(406, 253, 30, 30);
		contentPane.add(textPane_1_1);
		
		JLabel lblNewLabel_6 = new JLabel("= ESTA PERO VA EN OTRA POSICION");
		lblNewLabel_6.setBounds(186, 264, 210, 14);
		contentPane.add(lblNewLabel_6);
		
		JLabel lblNewLabel_7 = new JLabel("= NO ESTA");
		lblNewLabel_7.setBounds(446, 264, 63, 14);
		contentPane.add(lblNewLabel_7);
	}
}
